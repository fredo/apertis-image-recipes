osname = 'apertis'

release = "18.06"

docker_registry_name = 'docker-registry.apertis.org'
docker_image_name = "${docker_registry_name}/apertis/apertis-${release}-image-builder"

upload_dest = "archive@images.apertis.org:/srv/images/public"
upload_credentials = '5a23cd79-e26d-41bf-9f91-d756c131b811'

ostree_pull_url = "https://images.apertis.org/ostree/repo"
ostree_dest = "${upload_dest}/ostree/repo/"

test_repo_url = "git@gitlab.apertis.org:infrastructure/apertis-tests.git"
test_repo_credentials = 'df7b609b-df30-431d-a942-af263af80571'
test_repo_branch = 'master'
test_lava_credentials = 'apertis-lava-user'

demopack = "https://images.apertis.org/media/multimedia-demo.tar.gz"

sysroot_url_prefix = "https://images.apertis.org/sysroot/"

properties(
    [
        pipelineTriggers([cron('H 1 * * *')]),
    ]
)

def uploadDirectory(source, target, upload = true) {

  if (!upload) {
    println "Skipping upload of ${source} to ${target}"
    return
  }

  sshagent (credentials: [ upload_credentials, ] ) {
    env.NSS_WRAPPER_PASSWD = "/tmp/passwd"
    env.NSS_WRAPPER_GROUP = '/dev/null'
    sh(script: 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}')
    sh(script: "LD_PRELOAD=libnss_wrapper.so rsync -e \"ssh -oStrictHostKeyChecking=no\" -aP ${source} ${upload_dest}/${target}/")
  }
}

def runTestsJobs(image_name, profile_name, version, submit = true) {

  if (!submit) {
    println "Skipping submitting tests jobs for ${profile_name} ${version}"
    return
  }

  dir ("apertis-tests") {
    git(url: test_repo_url,
        poll: false,
        credentialsId: test_repo_credentials,
        branch: test_repo_branch)
  }

  withCredentials([ file(credentialsId: test_lava_credentials, variable: 'lqaconfig') ]) {
    sh(script: """\
    /usr/bin/lqa -c ${lqaconfig} submit \
    -g apertis-tests/templates/profiles.yaml \
    --profile ${profile_name} \
    -t image_date:${version} \
    -t image_name:${image_name}""")
  }

}

def buildImage(architecture, type, boards, debosarguments = "", sysroot = false, ostree = false, production = false) {
  return {
    node("docker-slave") {
      checkout scm
      docker.withRegistry("https://${docker_registry_name}") {
        buildenv = docker.image(docker_image_name)
        /* Pull explicitely to ensure we have the latest */
        buildenv.pull()

        buildenv.inside("--device=/dev/kvm") {
          stage("setup ${architecture} ${type}") {
            env.PIPELINE_VERSION = VersionNumber(versionNumberString: '${BUILD_DATE_FORMATTED,"yyyyMMdd"}.${BUILDS_TODAY_Z}')
            sh ("env ; mkdir -p ${PIPELINE_VERSION}/${architecture}/${type}")
          }

          try {

              stage("${architecture} ${type} ospack") {
                   sh(script: """\
                   cd ${PIPELINE_VERSION}/${architecture}/${type}; \
                   debos ${debosarguments} \
                    -t type:${type} \
                    -t architecture:${architecture} \
                    -t suite:${release} \
                    -t timestamp:${PIPELINE_VERSION} \
                    -t demopack:${demopack} \
                    -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz \
                    ${WORKSPACE}/${osname}-ospack.yaml""")
              }

              stage("${architecture} ${type} ospack upload") {
                  uploadDirectory (env.PIPELINE_VERSION, "daily/${release}", production)
              }

              // Loop through all boards for architecture
              for(String board: boards) {

                  // Build image name here so it can easily be re-used by phases.
                  def image_name = "${osname}_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}"

                  if (sysroot) {
                      sysrootfile = "sysroot-${osname}-${release}-${architecture}-${env.PIPELINE_VERSION}.tar.gz"
                      stage("${architecture} sysroot tarball") {
                          sh(script: """\
                              mkdir -p sysroot/${release}; \
                              cd sysroot/${release}; \
                              cp -l ${WORKSPACE}/${PIPELINE_VERSION}/${architecture}/${type}/ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz .; \
                              debos ${debosarguments} \
                                -t architecture:${architecture} \
                                -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz \
                                -t sysroot:${sysrootfile} \
                                ${WORKSPACE}/${osname}-sysroot.yaml; \
                              rm ospack*""")
                      }
                      stage("${architecture} generate sysroot metadata") {
                          writeFile file: "sysroot/${release}/sysroot-${osname}-${release}-${architecture}", text: "version=${release} ${PIPELINE_VERSION}\nurl=${sysroot_url_prefix}/${release}/${sysrootfile}"
                      }
                      stage("${architecture} sysroot upload") {
                          uploadDirectory ("sysroot/${release}/*", "sysroot/${release}", production)
                      }
                  }

                  stage("${architecture} ${type} ${board} image") {
                      sh(script: """\
                          cd ${PIPELINE_VERSION}/${architecture}/${type}; \
                          debos ${debosarguments} \
                            -t architecture:${architecture} \
                            -t type:${type} \
                            -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz \
                            -t image:${image_name} \
                            ${WORKSPACE}/${osname}-image-${board}.yaml""")
                  }

                  stage("${architecture} ${type} ${board} image upload") {
                      uploadDirectory (env.PIPELINE_VERSION, "daily/${release}", production)
                  }

                  if (ostree) {
                      buildOStree(architecture, type, board, debosarguments, production)
                      buildOStreeImage(architecture, type, board, debosarguments, production)
                      stage("${architecture} ${type} ${board} OStree repo cleanup") {
                          sh(script: """\
                              cd ${PIPELINE_VERSION}/${architecture}/${type}; \
                              rm -rf repo""")
                      }
                  }

                  // This stage must be the last in pipeline
                  // a failure to submit the tests would break the builds
                  stage("Submitting tests for ${architecture} ${type} ${board}") {
                      // TODO: Remove "uefi" once images run in the actual boards
                      def boardp = (board in ["uefi", "sdk"]) ? "qemu" : board

                      def profile_name = "${osname}-${release}-${type}-${architecture}-${boardp}"
                      runTestsJobs (image_name, profile_name, env.PIPELINE_VERSION, production)
                      if (ostree) {
                          profile_name = "${osname}_ostree-${release}-${type}-${architecture}-${boardp}"
                          image_name = "${osname}_ostree_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}"
                          runTestsJobs (image_name, profile_name, env.PIPELINE_VERSION, production)
                      }
                  }
              }
              if (ostree) {
                  /* Create ostree and ospack for container (board name = lxc) */
                  buildContainer(architecture, type, "lxc", debosarguments, production)

                  stage("${architecture} ${type} LXC OStree repo cleanup") {
                      sh(script: """\
                          cd ${PIPELINE_VERSION}/${architecture}/${type}; \
                          rm -rf repo""")
                      }
              }
          } finally {
            stage("Cleanup ${architecture} ${type}") {
              deleteDir()
            }
          }
        }
      }
    }
  }
}

def buildOStree(architecture, type, board, debosarguments = "", production = false) {
    stage("${architecture} ${type} ${board} OStree repo pull") {
        // Allow to work with not-existing branches
        // ostree-push below refuses to update ostree branch if `ostree pull` failed by any other reason
        sh(script: """\
           cd ${PIPELINE_VERSION}/${architecture}/${type}; \
           rm -rf repo; \
           mkdir repo; \
           ostree init --repo=repo --mode archive-z2;
           ostree remote --repo=repo add --no-gpg-verify origin ${ostree_pull_url};
           ostree pull --repo=repo origin ${osname}/${release}/${architecture}-${board}/${type} || true""")
    }

    stage("${architecture} ${type} ${board} OStree commit") {
        sh(script: """\
           cd ${PIPELINE_VERSION}/${architecture}/${type}; \
           debos ${debosarguments} \
           -t architecture:${architecture} \
           -t type:$type \
           -t board:$board \
           -t suite:$release \
           -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz \
           -t message:${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION} \
           ${WORKSPACE}/${osname}-ostree-commit.yaml;
           ostree --repo=repo summary -u""")
    }

    stage("${architecture} ${type} ${board} OStree push") {
        if (!production) {
            println "Skipping push of OStree to ${osname}/${architecture}-${board}/${type}"
            return
        }
        sshagent (credentials: [ upload_credentials, ] ) {
            env.NSS_WRAPPER_PASSWD = "/tmp/passwd"
                env.NSS_WRAPPER_GROUP = '/dev/null'
                sh(script: 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}')
                sh(script: "LD_PRELOAD=libnss_wrapper.so ostree-push --repo ${env.PIPELINE_VERSION}/${architecture}/${type}/repo/ ${ostree_dest} ${osname}/${release}/${architecture}-${board}/${type}")
        }
    }
}


def buildOStreeImage(architecture, type, board, debosarguments = "", production = false) {

    stage("${architecture} ${type} ${board} OStree image build") {
        sh(script: """\
           cd ${PIPELINE_VERSION}/${architecture}/${type}; \
           debos ${debosarguments} \
           -t architecture:${architecture} \
           -t type:$type \
           -t board:$board \
           -t suite:$release \
           -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz \
           -t image:apertis_ostree_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION} \
           -t message:${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION} \
           ${WORKSPACE}/apertis-ostree-image-${board}.yaml;""")
    }

    stage("${architecture} ${type} ${board} OStree image upload") {
        uploadDirectory ("${PIPELINE_VERSION}/${architecture}/${type}/apertis_ostree_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}.img.*",
                         "daily/${release}/${PIPELINE_VERSION}/${architecture}/${type}", production)
    }
}

def buildContainer(architecture, type, board, debosarguments = "", production = false) {

    buildOStree(architecture, type, board, debosarguments, production)

    stage("${architecture} ${type} ${board} OStree pack") {
        sh(script: """\
           cd ${PIPELINE_VERSION}/${architecture}/${type}; \
           debos ${debosarguments} \
           -t architecture:${architecture} \
           -t type:$type \
           -t suite:$release \
           -t repourl:${ostree_pull_url} \
           -t osname:${osname} \
           -t branch:${osname}/$release/${architecture}-${board}/${type} \
           -t ospack:${osname}_ostree_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}.tar.gz \
           -t message:${release}-${type}-${architecture}-${type}-${board}_${PIPELINE_VERSION} \
           ${WORKSPACE}/${osname}-ostree-pack.yaml""")
    }

    stage("${architecture} ${type} ${board} OStree upload") {
        uploadDirectory ("${PIPELINE_VERSION}/${architecture}/${type}/${osname}_ostree_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}.tar.gz",
                         "daily/${release}/${PIPELINE_VERSION}/${architecture}/${board}", production)
    }
}

/* Determine whether to run uploads based on the prefix of the job name; in
 * case of apertis we expect the official jobs under apertis-<release>/ while
 * non-official onces can be in e.g. playground/ */
def production = env.JOB_NAME.startsWith("${osname}-")

def images = [:]

images["Sdk"] = buildImage("amd64", "sdk", [ "sdk" ],
                           "--scratchsize 10G",
                           false, false, production)

// Types for all boards, common debos arguments, sysroots and ostree
def  types = [ [ "minimal", "", false, true],
               [ "target", "", false, true],
               [ "development", "--scratchsize 10G", true, false]
            ]

images += types.collectEntries { [ "Amd64 ${it[0]}": buildImage("amd64",
                                   it[0],
                                   ["uefi"],
                                   it[1],
                                   it[2],
                                   it[3],
                                   production ) ] }

images += types.collectEntries { [ "Arm64 ${it[0]}": buildImage("arm64",
                                   it[0],
                                   ["uboot"],
                                   it[1],
                                   it[2],
                                   it[3],
                                   production ) ] }

images += types.collectEntries { [ "Armhf ${it[0]}": buildImage("armhf",
                                   it[0],
                                   ["uboot", "mx6qsabrelite"],
                                   it[1],
                                   it[2],
                                   it[3],
                                   production ) ] }

parallel images
