LXC template generates configuration file for container, pre-mount hook and AppArmour profile.
Template is compatible with LXC upstream, so tools from LXC should be used to create/start/stop/destroy the container.

# Create container:
 sudo lxc-create -t $PWD/lxc-apertis-ostree --name apertis-test -- --ospack "https://images.apertis.org/lxc/17.12/20171230.0/lxc-ostree-17.12-amd64-minimal_20171230.0.tar.gz"

options:
 - -t -- use template named `lxc-apertis-ostree` from current directory
 - -P -- Use an alternate container path. The default is /var/lib/lxc
 - --name -- name of container
 - -- separator. Options for template must be added after this separator
 - --ospack -- URL to download the initial image

# Start the container in foreground mode:
 sudo lxc-start -F --name apertis-test

Pull the ostree, deploy and reboot into deployed OS tree:

 sudo ostree admin upgrade -r

# Destroy the container and associated configuration:
 sudo lxc-destroy --name apertis-test
