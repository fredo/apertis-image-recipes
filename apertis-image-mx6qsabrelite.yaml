{{ $architecture := or .architecture "armhf" }}
{{ $type := or .type "minimal" }}
{{ $mirror := or .mirror "https://repositories.apertis.org/apertis/" }}
{{ $suite := or .suite "18.06" }}
{{ $timestamp := or .timestamp "00000000.0" }}
{{ $ospack := or .ospack (printf "ospack_%s-%s-%s_%s.tar.gz" $suite $architecture $type $timestamp) }}
{{ $image := or .image (printf "apertis-%s-%s-%s_%s" $suite  $type $architecture $timestamp) }}

{{ $cmdline := or .cmdline " console=tty0 console=ttymxc1,115200n8 plymouth.ignore-serial-consoles" }}

architecture: {{ $architecture }}

actions:
  - action: unpack
    description: Unpack {{ $ospack }}
    compression: gz
    file: {{ $ospack }}

  - action: image-partition
    imagename: {{ $image }}.img
{{ if eq $type "minimal" }}
    imagesize: 4G
{{ else }}
    imagesize: 15G
{{end}}
    partitiontype: gpt
    gpt_gap: {{ sector 6144 }}b

    mountpoints:
      - mountpoint: /
        partition: system
        options: [ ro ]
      - mountpoint: /boot
        partition: boot
      - mountpoint: /home
        partition: general_storage

    partitions:
      - name: boot
        fs: ext2
        start: 6176s
        end: 256M
        flags: [ boot ]
      - name: system
        fs: btrfs
        start: 256M
{{ if eq $type "minimal" }}
        end: 2048M
{{ else }}
        end: 8192M
{{end}}
      - name: general_storage
        fs: btrfs
{{ if eq $type "minimal" }}
        start: 2048M
{{ else }}
        start: 8192M
{{end}}
        end: -4M

  - action: filesystem-deploy
    description: Deploying ospack onto image

  - action: run
    description: Add kernel parameters
    chroot: true
    command: sed -i '1 s/$/{{ $cmdline }}/' /etc/kernel/cmdline

  - action: apt
    description: Kernel and system packages
    packages:
      - kmod
      - linux-base
      - initramfs-tools
      - linux-image-armmp
      - u-boot-imx
      - u-boot-common
      - btrfs-tools

  - action: raw
    description: Install U-Boot
    origin: filesystem
    source: /usr/lib/u-boot/mx6qsabrelite/u-boot.imx
    offset: {{ sector 2 }}

  - action: run
    description: Cleanup /var/lib
    script: scripts/remove_var_lib_parts.binary

  - action: run
    description: Create block map for {{ $image }}.img
    postprocess: true
    command: bmaptool create {{ $image }}.img > {{ $image }}.img.bmap

  - action: run
    description: Compress {{ $image }}.img
    postprocess: true
    command: gzip -f {{ $image }}.img

  - action: run
    description: Checksum for {{ $image }}.img.gz
    postprocess: true
    command: md5sum {{ $image }}.img.gz > {{ $image }}.img.gz.md5
