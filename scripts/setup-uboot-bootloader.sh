#!/bin/bash

set -e

if [ -z  "${ROOTDIR}" ] ; then
  echo "ROOTDIR not given"
  exit 1
fi

mkdir -p ${ROOTDIR}/boot/extlinux
mkdir -p ${ROOTDIR}/boot/loader.0
ln -s loader.0  ${ROOTDIR}/boot/loader
ln -s ../loader/extlinux.conf ${ROOTDIR}/boot/extlinux/extlinux.conf
cat << EOF > ${ROOTDIR}/boot/extlinux/extlinux.conf
menu title Apertis OStree image
timeout 10

EOF
