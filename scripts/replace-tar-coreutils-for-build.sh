#!/bin/sh
set -e

cp /bin/rm /bin/tar /usr/local/bin
apt-get update
apt-get -y install coreutils tar
rm /usr/local/bin/rm /usr/local/bin/tar

